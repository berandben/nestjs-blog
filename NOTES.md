# NESTJS - BLOG

## Config

```bash
# docs
https://docs.nestjs.com/

#cli
npm i -g @nestjs/cli

# version
nest --version
10.1.18

```

## Hello World

```bash
# new project
nest new api

# run
cd api
npm run start:dev
# http://localhost:3000/
```

## ENV

```bash
# https://docs.nestjs.com/techniques/configuration
cd api
npm i --save @nestjs/config
```

## TypeORM / PostgreSQL

```bash
# https://docs.nestjs.com/techniques/database
cd api
npm install --save @nestjs/typeorm typeorm pg

# .env
DATABASE_URL=postgres://postgres:admin@127.0.0.1:5432/nestjsdb
```
